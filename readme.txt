date 13/04/2021

- Fluxes for pions and etas obtained using pythia 8.02 and HepMC v.2.06.09.

- Simulation details: Collision of protons at 120 GeV against carbon at rest.

- Files descriptions:

 * pion_flux_i.hepmc (i=20k,50k,100k,200k,1M)
 * eta_flux_i.hepmc (i=20k,50k,100k,200k,1M)

i correspond to the number of events considered in the pythia simulation
20k  = 20000 events
50k  = 50000 events
100k = 100000 events
200k = 200000 events
1M   = 1000000 events

- Cards description:

 For the event generation the flag  SoftQCD:all was considered. 
